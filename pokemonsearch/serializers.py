from rest_framework import serializers
from .models import Pokemon, Species, Stats, StatsTypes

# Serializers define the API representation.
class SpeciesSerializer(serializers.ModelSerializer):
    parent = serializers.StringRelatedField()
    class Meta:
        model = Species
        fields = ('id','name', 'parent')

class StatsTypesSerializer(serializers.ModelSerializer):
    class Meta:
        model = StatsTypes
        fields = ('name')

class StatsSerializer(serializers.ModelSerializer):
    stat_type = serializers.StringRelatedField()
    class Meta:
        model = Stats
        fields = ('stat_type', 'base_stat')

class PokemonSerializer(serializers.ModelSerializer): 
    stats_set = StatsSerializer(many=True)
    species = SpeciesSerializer()
    class Meta:
        model = Pokemon
        fields = ('id','name', 'height','weight', 'stats_set', 'species')