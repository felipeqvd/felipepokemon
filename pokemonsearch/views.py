from django.shortcuts import render
from rest_framework import routers, serializers, viewsets
from .serializers import PokemonSerializer, SpeciesSerializer, StatsSerializer, StatsTypesSerializer
from .models import Pokemon, Species, Stats, StatsTypes

class PokemonViewSet(viewsets.ModelViewSet):
    queryset = Pokemon.objects.all()
    serializer_class = PokemonSerializer

class SpeciesViewSet(viewsets.ModelViewSet):
    queryset = Species.objects.all()
    serializer_class = SpeciesSerializer

class StatsViewSet(viewsets.ModelViewSet):
    queryset = Stats.objects.all()
    serializer_class = StatsSerializer

class StatsTypesViewSet(viewsets.ModelViewSet):
    queryset = StatsTypes.objects.all()
    serializer_class = StatsTypesSerializer