from django.apps import AppConfig


class PokemonsearchConfig(AppConfig):
    name = 'pokemonsearch'
