from django.db import models


class Species(models.Model):
    name = models.CharField(max_length=200)    
    parent = models.ForeignKey('self', on_delete=models.CASCADE, null=True)

    def __str__(self):
        return self.name

class Pokemon(models.Model):
    name = models.CharField(max_length=200)
    height = models.IntegerField(default=0)
    weight = models.IntegerField(default=0)
    species = models.ForeignKey(Species, on_delete=models.CASCADE)

    def __str__(self):
        return self.name
        
class StatsTypes(models.Model):
    name = models.CharField(max_length=200)

    def __str__(self):
        return self.name

class Stats(models.Model):
    pokemon = models.ForeignKey(Pokemon, on_delete=models.CASCADE)
    stat_type = models.ForeignKey(StatsTypes, on_delete=models.CASCADE)
    base_stat = models.IntegerField(default=0)