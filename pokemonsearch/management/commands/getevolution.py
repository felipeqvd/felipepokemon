from django.core.management.base import BaseCommand, CommandError
from pokemonsearch.models import StatsTypes, Stats, Pokemon, Species
import requests

class Command(BaseCommand):
    help = 'Get an evolution chain and store the information in the database'

    def add_arguments(self, parser):
        parser.add_argument('evolution_chain_id', type=int)

    def store_stats(self, pokemon_stats, pokemon):
        stats_message = ""
        for stat in pokemon_stats:
            try:
                stat_obj = StatsTypes.objects.get(name=stat['stat']['name'])
            except StatsTypes.DoesNotExist:
                stat_info = requests.get(stat['stat']['url'])
                stat_info_obj = stat_info.json()
                stat_obj = StatsTypes(id = stat_info_obj['id'], name = stat_info_obj['name'])
                stat_obj.save()
            stored_stat, created = Stats.objects.update_or_create(
                pokemon = pokemon, stat_type = stat_obj,
                defaults={'base_stat': stat['base_stat']},
            )
            stat_message = stat['stat']['name']+"="+str(stat['base_stat'])+", "
            stats_message = stats_message+stat_message
        return stats_message

    def store_pokemon(self, pokemon_varieties, species):
        pokemon_messages = ""
        for pokemon in pokemon_varieties:
            pokemon_info = requests.get(pokemon['pokemon']['url'])
            pokemon_info_obj = pokemon_info.json()
            stored_pokemon, created = Pokemon.objects.update_or_create(
                id = pokemon_info_obj['id'],
                defaults={'name': pokemon_info_obj['name'], 'height': pokemon_info_obj['height'], 'weight': pokemon_info_obj['weight'], 'species': species},
            )
            pokemon_message = pokemon_info_obj['name']+": height="+str(pokemon_info_obj['height'])+", weight="+str(pokemon_info_obj['weight'])+", id="+str(pokemon_info_obj['id'])+", "
            stats_message = self.store_stats(pokemon_info_obj['stats'], Pokemon.objects.get(id=pokemon_info_obj['id']))
            pokemon_messages = pokemon_messages + pokemon_message + stats_message + "\n"
        return pokemon_messages

    def store_species(self, species, parent):        
        species_info = requests.get(species['url'])
        species_info_obj = species_info.json()
        stored_species, created = Species.objects.update_or_create(
            id = species_info_obj['id'],
            defaults={'name': species_info_obj['name'], 'parent': parent},
        )
        pokemon_messages = self.store_pokemon(species_info_obj['varieties'], Species.objects.get(id=species_info_obj['id']))
        response = [pokemon_messages,stored_species]
        return response

    def read_evolutions(self, evolves_to, parent):
        recursive_message = ""
        if evolves_to == []:
            return ""
        else:
            for evolution in evolves_to:
                species_response = self.store_species(evolution['species'], parent)
                species = species_response[1]
                message = self.read_evolutions(evolution['evolves_to'], species)
                recursive_message = recursive_message + species_response[0] + message
        return recursive_message

    def handle(self, *args, **options):
        response = requests.get('https://pokeapi.co/api/v2/evolution-chain/'+str(options['evolution_chain_id']))
        chain = response.json()
        first_species_response = self.store_species(chain['chain']['species'], None)
        other_species_message = self.read_evolutions(chain['chain']['evolves_to'], first_species_response[1])
        self.stdout.write("Stored information:")
        self.stdout.write(first_species_response[0])
        self.stdout.write(other_species_message)