from django.core.management.base import BaseCommand, CommandError
from pokemonsearch.models import StatsTypes as Type
import requests

class Command(BaseCommand):
    help = 'Update stats types in the database'

    def handle(self, *args, **options):
        response = requests.get('https://pokeapi.co/api/v2/stat')
        stats = response.json()
        for index in range(len(stats['results'])):
            stat_type = Type(id = index + 1, name = stats['results'][index]['name'])
            stat_type.save()
            self.stdout.write(str(stat_type.id), ending=' ')